﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{ 
    class Stock: IObservable
    {
        StockInfo stockInfo;
        List<IObserver> observers;

        public Stock(StockInfo stockInfo)
        {
            this.stockInfo = stockInfo;
            observers = new List<IObserver>();
        }

        public void RegisterObserver(IObserver observer)
        {
            observers.Add(observer);
        }

        public void UnRegisterObserver(IObserver observer)
        {
            observers.Remove(observer);
        }

        public void NotifyObserver()
        {
            foreach (IObserver observer in observers)
            {
                observer.Update(stockInfo);
            }
        }

        public void Market()
        {
            Random rnd = new Random();
            stockInfo.USD = rnd.Next(70, 90);
            stockInfo.EURO = rnd.Next(80, 100);
            NotifyObserver();
        }
    }
}

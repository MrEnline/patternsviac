﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    class StockInfo
    {
        public int USD { get; set; }
        public int EURO { get; set; }
    }
}

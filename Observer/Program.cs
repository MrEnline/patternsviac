﻿using System;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            StockInfo stockInfo = new StockInfo();
            Stock stock = new Stock(stockInfo);
            Bank bank = new Bank("Рога и копыта", stock);
            Broker broker = new Broker("Петрович", stock);
            stock.Market();
            broker.UnRegister();
            stock.Market();

            Console.ReadKey();
        }
    }
}

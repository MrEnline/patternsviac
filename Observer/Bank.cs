﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    class Bank: IObserver
    {
        IObservable observable;
        string name;

        public Bank(string name, IObservable observable)
        {
            this.name = name;
            this.observable = observable;
            this.observable.RegisterObserver(this);
        }

        public void Update(Object o)
        {
            StockInfo stockInfo = (StockInfo)o;

            if (stockInfo.USD > 80)
                Console.WriteLine($"Банк {name} продает доллары по курсу {stockInfo.USD}");
            else
                Console.WriteLine($"Банк {name} покупает доллары по курсу {stockInfo.USD}");

            if (stockInfo.EURO > 80)
                Console.WriteLine($"Банк {name} продает евро по курсу {stockInfo.EURO}");
            else
                Console.WriteLine($"Банк {name} покупает евро по курсу {stockInfo.EURO}");
        }

        public void UnRegister()
        {
            observable.UnRegisterObserver(this);
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Text;

namespace State
{
    abstract class State
    {
        protected string Name { get; set; }
        abstract public void Handle(Context context);

        public string GetName() { return Name; }
    }

    class StateA: State
    {
        public StateA()
        {
            Name = "А";
        }

        public override void Handle(Context context)
        {
            context.state = new StateB();
            Console.WriteLine($"Состояние изменилось с {Name} на {context.state.GetName()}");
        }
    }

    class StateB: State
    {
        public StateB()
        {
            Name = "Б";
        }

        public override void Handle(Context context)
        {
            context.state = new StateA();
            Console.WriteLine($"Состояние изменилось с {Name} на {context.state.GetName()}");
        }
    }
}

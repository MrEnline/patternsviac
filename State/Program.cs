﻿using System;

namespace State
{
    class Program
    {
        static void Main(string[] args)
        {
            StateA stateA = new StateA();
            StateB stateB = new StateB();
            Context context = new Context(stateA);
            context.Request();
            context.Request();

            Console.ReadKey();
        }
    }
}

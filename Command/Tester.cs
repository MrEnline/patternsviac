﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command
{
    class Tester
    {
        public void StartTest()
        {
            Console.WriteLine("Тестер начал тестировать");
        }

        public void StopTest()
        {
            Console.WriteLine("Тестер закончил тестировать");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command
{
    class Marketolog
    {
        public void StartMarket()
        {
            Console.WriteLine("Маркетолог начал рекламировать товар");
        }

        public void StopMarket()
        {
            Console.WriteLine("Маркетолог закончил рекламную кампанию товара");
        }
    }
}

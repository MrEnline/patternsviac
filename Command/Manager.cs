﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command
{
    class Manager
    {
        ICommand command;

        public void SetCommand(ICommand command)
        {
            this.command = command;
        }

        public void StartProject()
        {
            command.Execute();
        }

        public void StopProject()
        {
            command.Undo();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command
{
    class CommandMarket: ICommand
    {
        Marketolog marketolog;

        public CommandMarket(Marketolog marketolog)
        {
            this.marketolog = marketolog;
        }

        public void Execute()
        {
            marketolog.StartMarket();
        }

        public void Undo()
        {
            marketolog.StopMarket();
        }
    }
}

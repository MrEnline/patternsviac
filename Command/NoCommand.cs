﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command
{
    class NoCommand:ICommand
    {
        public void Execute() { }
        public void Undo() { }
    }
}

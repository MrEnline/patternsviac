﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command
{
    class CommandProg: ICommand
    {
        Programmist programmist;

        public CommandProg(Programmist programmist)
        {
            this.programmist = programmist;
        }

        public void Execute()
        {
            programmist.StartCoding();
        }

        public void Undo()
        {
            programmist.StopCoding();
        }
    }
}

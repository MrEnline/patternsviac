﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command
{
    class Programmist
    {
        public void StartCoding()
        {
            Console.WriteLine("Программист начал кодить");
        }

        public void StopCoding()
        {
            Console.WriteLine("Программист закончил кодить");
        }
    }
}

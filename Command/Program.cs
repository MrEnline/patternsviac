﻿using System;
using System.Collections.Generic;

namespace Command
{
    class Program
    {
        static void Main(string[] args)
        {
            Programmist programmist = new Programmist();
            Tester tester = new Tester();
            Marketolog marketolog = new Marketolog();
            Manager manager = new Manager();
            List<ICommand> commands = new List<ICommand>()
            {
                new CommandProg(programmist),
                new CommandTest(tester),
                new CommandMarket(marketolog)
            };
            MultyCommand multy = new MultyCommand(commands);
            manager.SetCommand(multy);
            manager.StartProject();
            Console.WriteLine(new string('-', 50));
            manager.StopProject();
            Console.ReadKey();
        }
    }
}

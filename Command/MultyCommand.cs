﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command
{
    class MultyCommand: ICommand
    {
        List<ICommand> commands;

        public MultyCommand(List<ICommand> commands)
        {
            this.commands = commands;
        }

        public void Execute()
        {
            foreach (ICommand command in commands)
                command.Execute();
        }

        public void Undo()
        {
            foreach (ICommand command in commands)
                command.Undo();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command
{
    class CommandTest: ICommand
    {
        Tester tester;

        public CommandTest(Tester tester)
        {
            this.tester = tester;
        }

        public void Execute()
        {
            tester.StartTest();
        }

        public void Undo()
        {
            tester.StopTest();
        }
    }
}

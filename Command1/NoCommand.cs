﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command1
{
    class NoCommand: ICommand
    {
        public void Execute() { }
        public void Undo() { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command1
{
    class CommandTV: ICommand
    {
        TV tv;

        public CommandTV(TV tv)
        {
            this.tv = tv;
        }

        public void Execute()
        {
            tv.OnTV();
        }

        public void Undo()
        {
            tv.OffTV();
        }
    }
}

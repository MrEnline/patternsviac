﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command1
{
    class TV
    {
        public void OnTV()
        {
            Console.WriteLine("Включить телек");
        }

        public void OffTV()
        {
            Console.WriteLine("Отключить телек");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command1
{
    class Volume
    {
        private const int High = 20;
        private const int Off = 0;
        private int level;

        public Volume()
        {
            level = Off;
        }

        public void RaiseLevel()
        {
            if (level < High)
                level++;
            Console.WriteLine($"Уровень звука {level}");
        }

        public void DropLevel()
        {
            if (level > Off)
                level--;
            Console.WriteLine($"Уровень звука {level}");
        }
    }
}

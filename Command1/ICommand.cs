﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command1
{
    interface ICommand
    {
        void Execute();
        void Undo();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command1
{
    class MultyPult
    {
        Stack<ICommand> commandsHistory;
        ICommand[] buttons;

        public MultyPult()
        {
            buttons = new ICommand[2];
            for (int i = 0; i < buttons.Length; i++)
                buttons[i] = new NoCommand();
            commandsHistory = new Stack<ICommand>();        //команды добавляются в Стэк для истории
        }

        public void SetCommand(int numCommand, ICommand command)
        {
            buttons[numCommand] = command;
        }

        public void PressButtonPult(int numCommand)
        {
            buttons[numCommand].Execute();
            commandsHistory.Push(buttons[numCommand]);
        }

        public void UndoButtonPult()
        {
            if (commandsHistory.Count > 0)
            { 
                ICommand undoCommand = commandsHistory.Pop();
                undoCommand.Undo();
            }
        }

        public void OffTV()
        {
            commandsHistory.Clear();
            buttons[0].Undo();
            buttons[0] = new NoCommand();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command1
{
    class CommandVolume: ICommand
    {
        Volume volume;

        public CommandVolume(Volume volume)
        {
            this.volume = volume;
        }

        public void Execute()
        {
            volume.RaiseLevel();
        }

        public void Undo()
        {
            volume.DropLevel();
        }
    }
}

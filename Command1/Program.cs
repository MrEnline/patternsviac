﻿using System;

namespace Command1
{
    class Program
    {
        static void Main(string[] args)
        {
            Volume volume = new Volume();
            TV tv = new TV();
            MultyPult multyPult = new MultyPult();
            multyPult.SetCommand(0, new CommandTV(tv));
            multyPult.SetCommand(1, new CommandVolume(volume));
            multyPult.PressButtonPult(0);
            multyPult.PressButtonPult(1);
            multyPult.PressButtonPult(1);
            multyPult.PressButtonPult(1);
            multyPult.PressButtonPult(1);
            multyPult.UndoButtonPult();
            multyPult.OffTV();
            Console.ReadKey();
        }
    }
}
